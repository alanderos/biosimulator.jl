using BioSimulator
using Base.Test

function kendall_mean(i, t, α, μ, ν)
  x = exp((α - μ) * t)
  return i * x + ν / (α - μ) * (x - 1)
end

i = 5
α = 2.0
μ = 1.0
ν = 0.5

t = 4.0
n = 100
u = 5357
m = 100_000

model = kendall(i, α, μ, ν)

theoretical = kendall_mean(i, linspace(0.0, t, n + 1), α, μ, ν)

algorithms = [SSA, FRM, NRM, ODM, SAL]

# Run SSA and SAL once to compile
print("    Precompiling..."); @time begin
  for algorithm in algorithms
    run_test(model, algorithm, t, n, 1)
  end
end

print("    Running tests...\n\n")
for algorithm in algorithms
  print("   - $(split(uppercase(string(algorithm)),".")[2]): ")

  srand(u)

  @time result = run_test(model, algorithm, t, n, m)

  # count the number of relative errors that lie outside the interval [0.98, 1.02]
  observed = reshape(mean(result.data, 3), n+1)
  relative = observed ./ theoretical
  badness = count(x -> !isapprox(x, 1.0, rtol=0.4), relative)

  @test badness / n ≤ 0.05

  print("     % bad estimates = ", badness / n, " out of $(n)\n")
  println()
end
